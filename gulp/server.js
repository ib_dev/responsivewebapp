'use strict';

var nodemon = require('gulp-nodemon'); // minu

// minu
module.exports = function (gulp) {
  gulp.task('server',function(){
    nodemon({
      script: 'server/server.js',
      ext: 'js',
      env: { 'NODE_ENV': 'development' },
      nodeArgs: ['--debug'],
      watch: [
        'server/**'
      ]
    })
    .on('restart', function () {
      console.log('restarted!')
    });

  });
};


