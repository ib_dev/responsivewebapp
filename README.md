# Overview #

Sample of responsive web app front-end. Fully modular angular app design. Directory structure geared towards large projects. Each controller, service, filter and directive are placed in their own file. Works with various mobile and desktop browsers (also with some very old versions). Front-end, no database attached.

## How to use ##

Two videos to present the usage possibilities:

* [Products part](https://responsivewebappvideos.firebaseapp.com/products/)
* [Footer part](https://responsivewebappvideos.firebaseapp.com/footer/)

## Test it ##

* [Demo](http://responsive-web-app.tk/#/admin)

## Most important technologies and methodology used ##

* AngularJs - JavaScript-based front-end web application framework.
* Generator-ng-poly - Yeoman generator for modular AngularJS apps with optional Polymer support
* Bootstrap - HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.
* Sass - CSS extension language.
* BEM - Methodology, that helps you to achieve reusable components and code sharing in the front-end.
* Gulp - Node.js-based task runner.
* Bower - A package manager for the web.

## Tested and working with ##

* Internet Explorer 9*
* Safari 5*
* Chrome 49*
* Firefox 31*
* Opera 12*
* Android 4.1
* Android 6
* Windows Phone 8

*Tested on PC running Windows operating system.

## Setup ##
1. Install [Node.js](http://nodejs.org/)
- This will also install npm.
1. Run `npm install -g bower gulp yo generator-ng-poly@0.13.0`
- This enables Bower, Gulp, and Yeoman generators to be used from command line.
1. Run `npm install` to install this project's dependencies
1. Run `bower install` to install client-side dependencies
1. Use [generator-ng-poly](https://github.com/dustinspecker/generator-ng-poly) to create additional components

## Gulp tasks ##
- Run `gulp` to start a localhost and open in the default browser
- Run `gulp build` to compile assets
- Run `gulp build --stage=prod` to concat and minify HTML, CSS, and Angular modules
- Run `gulp dev` to run the build task and setup the development environment
