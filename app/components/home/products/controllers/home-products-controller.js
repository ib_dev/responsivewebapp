(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name home.products.controller:HomeProductsCtrl
   *
   * @description
   *
   */
  angular
    .module('home.products')
    .controller('HomeProductsCtrl', HomeProductsCtrl);

  function HomeProductsCtrl(ProductsCommon, $controller) {
    var vm = this;

    // instantiate base controller
    $controller('HomeAdminProductsBaseCtrl', { vm: vm });
  }
}());
