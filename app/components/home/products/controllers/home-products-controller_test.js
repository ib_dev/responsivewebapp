/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('HomeProductsCtrl', function () {
  var ctrl;

  beforeEach(module('home.products'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('HomeProductsCtrl');
  }));

  it('should have ctrlName as HomeProductsCtrl', function () {
    expect(ctrl.ctrlName).toEqual('HomeProductsCtrl');
  });
});
