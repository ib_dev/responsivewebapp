/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('homeProducts', function () {
  var scope
    , element;

  beforeEach(module('home.products', 'components/home/products/directives/home-products-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<home-products></home-products>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().homeProducts.name).toEqual('homeProducts');
  });
});
