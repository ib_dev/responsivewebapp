(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name home.products.directive:homeProducts
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="home.products">
       <file name="index.html">
        <home-products></home-products>
       </file>
     </example>
   *
   */
  angular
    .module('home.products')
    .directive('homeProducts', homeProducts);

  function homeProducts() {
    return {
      restrict: 'EA',
      scope: {
        productsData: "="
      },
      templateUrl: 'components/home/products/directives/home-products-directive.tpl.html',
      replace: false,
      bindToController: true,
      controllerAs: 'vm',
      controller: 'HomeProductsCtrl',
      link: function (scope, element, attrs) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */
      }
    };
  }
}());
