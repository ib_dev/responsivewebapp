/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('homeFooter', function () {
  var scope
    , element;

  beforeEach(module('home.footer', 'components/home/footer/directives/home-footer-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<home-footer></home-footer>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().homeFooter.name).toEqual('homeFooter');
  });
});
