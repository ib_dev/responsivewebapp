(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name home.footer.directive:homeFooter
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="home.footer">
       <file name="index.html">
        <home-footer></home-footer>
       </file>
     </example>
   *
   */
  angular
    .module('home.footer')
    .directive('homeFooter', homeFooter);

  function homeFooter() {
    return {
      restrict: 'EA',
      scope: {
        footerData: "="
      },
      templateUrl: 'components/home/footer/directives/home-footer-directive.tpl.html',
      replace: false,
      bindToController: true,
      controllerAs: 'vm',
      controller: function () {},
      link: function (scope, element, attrs) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */
      }
    };
  }
}());
