(function () {
  'use strict';

  /* @ngdoc object
   * @name components.home
   * @description
   *
   */
  angular
    .module('components.home', [
      'home.footer',
      'home.products'
    ]);
}());
