(function () {
  'use strict';

  /**
   * @ngdoc filter
   * @name components.homeAdminCommon.filter:addCurrencySymbol
   *
   * @description
   *
   * @param {Array} input The array to filter
   * @returns {Array} The filtered array
   *
   */
  angular
    .module('components.homeAdminCommon')
    .filter('addCurrencySymbol', addCurrencySymbol);

  function addCurrencySymbol() {
    return function(input, place) {

      // Ensure that we are working with a number
      if(isNaN(input) || input == '') {
        return input;
      } else {

        var symbol = '€';
        var place = place === undefined ? true : place;

        // Perform the operation to set the symbol in the right location
        if( place === true) {
          return symbol + input;
        } else {
          return input + symbol;
        }

      }
    }
  }
}());
