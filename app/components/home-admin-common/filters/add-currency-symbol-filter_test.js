/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('addCurrencySymbol', function () {
  beforeEach(module('components.homeAdminCommon'));

  it('should filter our numbers not greater than 3', inject(function ($filter) {
    expect($filter('addCurrencySymbol')([1, 2, 3, 4])).toEqual([4]);
  }));
});
