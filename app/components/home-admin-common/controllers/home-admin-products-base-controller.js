(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name components.homeAdminCommon.controller:HomeAdminProductsBaseCtrl
   *
   * @description
   *
   */
  angular
    .module('components.homeAdminCommon')
    .controller('HomeAdminProductsBaseCtrl', HomeAdminProductsBaseCtrl);

  function HomeAdminProductsBaseCtrl(vm, ProductsCommon) {
    vm.groupNames = angular.copy( vm.productsData );

    vm.group = ProductsCommon.load(vm.groupNames);

    vm.changeCategory = function(index){
      vm.group = ProductsCommon.changeCategory(index, vm.groupNames);
    }
  }
}());
