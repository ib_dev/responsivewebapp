/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('HomeAdminProductsBaseCtrl', function () {
  var ctrl;

  beforeEach(module('components.homeAdminCommon'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('HomeAdminProductsBaseCtrl');
  }));

  it('should have ctrlName as HomeAdminProductsBaseCtrl', function () {
    expect(ctrl.ctrlName).toEqual('HomeAdminProductsBaseCtrl');
  });
});
