/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('ProductsCommon', function () {
  var service;

  beforeEach(module('components.homeAdminCommon'));

  beforeEach(inject(function (ProductsCommon) {
    service = ProductsCommon;
  }));

  it('should equal ProductsCommon', function () {
    expect(service.get()).toEqual('ProductsCommon');
  });
});
