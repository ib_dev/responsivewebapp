(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name components.homeAdminCommon.factory:ProductsCommon
   *
   * @description
   *
   */
  angular
    .module('components.homeAdminCommon')
    .factory('ProductsCommon', ProductsCommon);

  function ProductsCommon() {
    var vm = {};

    vm.setActive = function(index, arr){
      for (var i = 0; i < arr.length; i++) {
        arr[i].active = false;
      }
      arr[index].active = true;
    }

    vm.findActive = function(arr){
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].active == true) {
          return i;
        }
      }
    }

    vm.load = function(arr){
      var index = vm.findActive(arr);
      return vm.groupData(index, arr);
    }

    vm.changeCategory = function(index, arr){
      // Check if any category
      if (!arr.length) return false;

      if (index == arr.length) index--; // Decrement index if last category removed

      vm.setActive(index, arr);
      return vm.groupData(index, arr);
    }

    vm.groupData = function(index, arr){
      return arr[index].products;
    }

    return vm;
  }
}());
