(function () {
  'use strict';

  /* @ngdoc object
   * @name components
   * @description
   *
   */
  angular
    .module('components', [
      'components.admin',
      'components.home',
      'components.homeAdminCommon'
    ]);
}());
