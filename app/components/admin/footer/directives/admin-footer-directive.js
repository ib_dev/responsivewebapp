(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name admin.footer.directive:adminFooter
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="admin.footer">
       <file name="index.html">
        <admin-footer></admin-footer>
       </file>
     </example>
   *
   */
  angular
    .module('admin.footer')
    .directive('adminFooter', adminFooter);

  function adminFooter() {
    return {
      restrict: 'E',
      scope: {
        disable: "=",
        footerData: "="
      },
      templateUrl: 'components/admin/footer/directives/admin-footer-directive.tpl.html',
      replace: false,
      bindToController: true,
      controllerAs: 'vm',
      controller: 'AdminFooterCtrl'
    };
  }
}());
