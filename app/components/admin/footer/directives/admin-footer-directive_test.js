/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('adminFooter', function () {
  var scope
    , element;

  beforeEach(module('admin.footer', 'components/admin/footer/directives/admin-footer-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<admin-footer></admin-footer>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().adminFooter.name).toEqual('adminFooter');
  });
});
