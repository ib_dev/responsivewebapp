(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name admin.footer.controller:AdminFooterCtrl
   *
   * @description
   *
   */
  angular
    .module('admin.footer')
    .controller('AdminFooterCtrl', AdminFooterCtrl);

  function AdminFooterCtrl(AdminCommon, AdminFooter) {
    var vm = this;

    vm.data = angular.copy( vm.footerData );

    vm.putLive = function(){
      // copying array without breaking the reference
      angular.copy(vm.data, vm.footerData);
    }

    vm.disable = false;

    vm.newcol = AdminFooter.emptyCol();

    vm.edit = function(col){
      col.active = true; // changes column color what is edited

      vm.newcol = angular.copy( col );

      vm.disable = true;
    }

    vm.save = function(form){
      delete vm.newcol.active;

      AdminCommon.save(vm.newcol, vm.data);

      vm.newcol = AdminFooter.emptyCol();

      vm.disable = false;

      form.$setPristine(); // removes errors
    }

    vm.removeCol = function(id, arr){
      AdminCommon.deleteById(id, arr);
    }

    vm.addLink = function(){
      AdminFooter.addLink(vm.newcol.links);
    }

    vm.removeLink = function(item, arr){
      AdminCommon.deleteByItem(item, arr);
    }

    /* Angular ui sortable */
    vm.sortableOptions = {
      revert: true, // Add animation
      delay: 100, // Time in milliseconds to define when the sorting should start
      distance: 10, // Tolerance, in pixels, for when sorting should start
      tolerance: "pointer",
      //containment: "parent",
      start: function( event, ui ) {
        AdminCommon.dragged = true;
      }
    };
  }
}());
