/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AdminFooterCtrl', function () {
  var ctrl;

  beforeEach(module('admin.footer'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('AdminFooterCtrl');
  }));

  it('should have ctrlName as AdminFooterCtrl', function () {
    expect(ctrl.ctrlName).toEqual('AdminFooterCtrl');
  });
});
