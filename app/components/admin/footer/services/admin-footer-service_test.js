/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AdminFooter', function () {
  var service;

  beforeEach(module('admin.footer'));

  beforeEach(inject(function (AdminFooter) {
    service = AdminFooter;
  }));

  it('should equal AdminFooter', function () {
    expect(service.get()).toEqual('AdminFooter');
  });
});
