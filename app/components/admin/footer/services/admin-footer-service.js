(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name admin.footer.factory:AdminFooter
   *
   * @description
   *
   */
  angular
    .module('admin.footer')
    .factory('AdminFooter', AdminFooter);

  function AdminFooter() {
    var vm = {};

    vm.emptyCol = function(){
      return  { heading: '', links: [
        { path: '', name: '' },
        { path: '', name: '' }
      ]};
    }

    vm.addLink = function(arr){
      arr.push( { path: '', name: '' } );
    }

    return vm;
  }
}());
