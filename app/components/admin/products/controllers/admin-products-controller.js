(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name admin.products.controller:AdminProductsCtrl
   *
   * @description
   *
   */
  angular
    .module('admin.products')
    .controller('AdminProductsCtrl', AdminProductsCtrl);

  function AdminProductsCtrl(AdminProducts, ProductsCommon, AdminCommon, AdminProductsModals, $controller) {
    var vm = this;

    // instantiate base controller
    $controller('HomeAdminProductsBaseCtrl', { vm: vm });

    /*** Products Edit ***/
    vm.putLive = function(){

      AdminProductsModals.open(vm.groupNames).then(
        function(index) {
          ProductsCommon.setActive(index, vm.groupNames);

          // copying array without breaking the reference
          angular.copy(vm.groupNames, vm.productsData);
        },
        function(msg) {
          //console.log(msg);
        }
      );

    }

    vm.edit = function(obj, type){
      vm.newobj = AdminProducts.edit(obj, type);
      vm.disable = true;
    }

    vm.save = function(group, form){

      // Check if new category
      if (!vm.newobj.hasOwnProperty('id') && form.$name == 'editCategoryForm') {
        vm.newobj.active = false;
        vm.newobj.products = [];

        saveAndSetPristine();

        // Check if first category
        if (group.length == 1) {
          vm.group = ProductsCommon.changeCategory(0, group);
        }

      } else {
        saveAndSetPristine();
      }


      function saveAndSetPristine(){
        delete vm.newobj.uiActive;
        AdminCommon.save(vm.newobj, group);
        vm.newobj = {};

        vm.disable = false;

        form.$setPristine(); // removes errors
      }

    }

    vm.removeObj = function(id, arr){ // delete product or category
      AdminCommon.deleteById(id, arr);
    }

    /* Angular ui sortable */
    vm.sortableOptions = {
      revert: true, // Add animation
      delay: 100, // Time in milliseconds to define when the sorting should start
      distance: 10, // Tolerance, in pixels, for when sorting should start
      tolerance: "pointer",
      //containment: "window", // Area where draging is allowed
      start: function() {
        AdminCommon.dragged = true;
      }
    };

  }
}());
