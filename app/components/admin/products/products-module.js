(function () {
  'use strict';

  /* @ngdoc object
   * @name admin.products
   * @description
   *
   */
  angular
    .module('admin.products', [
      'products.modals'
    ]);
}());
