(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name products.modals.factory:AdminProductsModals
   *
   * @description
   *
   */
  angular
    .module('products.modals')
    .factory('AdminProductsModals', AdminProductsModals);

  function AdminProductsModals($uibModal, $q) {
    var vm = {};

    var makeArrOfProps = function(arr, prop){
      var propArr = [];
      for (var i = 0; i < arr.length; i++) {
        var obj = {};
        obj[prop] = arr[i][prop];
        propArr.push( obj );
      }
      return propArr;
    }

    vm.open = function (arr) {
      var deferred = $q.defer();

      var list = makeArrOfProps(arr, 'category');

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'components/admin/products/modals/views/setActiveModal.tpl.html',
        controller: 'SetActiveModalCtrl', // Modal instance ctrl
        bindToController: true,
        controllerAs: 'vm',
        size: 'sm',
        resolve: {
          items: function () {
            return list;
          }
        }
      });

      modalInstance.result.then(function (index) {
        deferred.resolve(index);
      }, function (msg) {
        deferred.reject('Modal ' + msg);
      });

      return deferred.promise;
    };

    return vm;
  }
}());
