/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AdminProductsModals', function () {
  var service;

  beforeEach(module('products.modals'));

  beforeEach(inject(function (AdminProductsModals) {
    service = AdminProductsModals;
  }));

  it('should equal AdminProductsModals', function () {
    expect(service.get()).toEqual('AdminProductsModals');
  });
});
