(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name products.modals.controller:SetActiveModalCtrl
   *
   * @description
   *
   */
  angular
    .module('products.modals')
    .controller('SetActiveModalCtrl', SetActiveModalCtrl);

  function SetActiveModalCtrl($uibModalInstance, items) {
    var vm = this;

    vm.items = items;

    vm.save = function () {
      if(vm.selectedCategory) $uibModalInstance.close(
        vm.items.indexOf(vm.selectedCategory)
      );
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('canceled');
    };
  }
}());
