/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('SetActiveModalCtrl', function () {
  var ctrl;

  beforeEach(module('products.modals'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('SetActiveModalCtrl');
  }));

  it('should have ctrlName as SetActiveModalCtrl', function () {
    expect(ctrl.ctrlName).toEqual('SetActiveModalCtrl');
  });
});
