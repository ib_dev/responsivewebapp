(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name admin.products.factory:AdminProducts
   *
   * @description
   *
   */
  angular
    .module('admin.products')
    .factory('AdminProducts', AdminProducts);

  function AdminProducts() {
    var vm = {};

    vm.edit = function(obj, type){
      // add property dynamically (ES6 only)
      //obj.uiActive = { [type] : true };

      // add property dynamically
      obj.uiActive = {};
      Object.defineProperty(obj.uiActive, type, {value : true,
                                                 writable : true,
                                                 enumerable : true,
                                                 configurable : true});

      return angular.copy(obj);
    }

    return vm;
  }
}());
