/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AdminProducts', function () {
  var service;

  beforeEach(module('admin.products'));

  beforeEach(inject(function (AdminProducts) {
    service = AdminProducts;
  }));

  it('should equal AdminProducts', function () {
    expect(service.get()).toEqual('AdminProducts');
  });
});
