(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name admin.products.directive:adminProducts
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="admin.products">
       <file name="index.html">
        <admin-products></admin-products>
       </file>
     </example>
   *
   */
  angular
    .module('admin.products')
    .directive('adminProducts', adminProducts);

  function adminProducts() {
    return {
      restrict: 'E',
      scope: {
        disable: "=",
        productsData: "="
      },
      templateUrl: 'components/admin/products/directives/admin-products-directive.tpl.html',
      replace: false,
      bindToController: true,
      controllerAs: 'vm',
      controller: 'AdminProductsCtrl'
    };
  }
}());
