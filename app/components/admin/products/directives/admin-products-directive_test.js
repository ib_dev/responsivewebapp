/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('adminProducts', function () {
  var scope
    , element;

  beforeEach(module('admin.products', 'components/admin/products/directives/admin-products-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<admin-products></admin-products>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().adminProducts.name).toEqual('adminProducts');
  });
});
