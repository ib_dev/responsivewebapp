(function () {
  'use strict';

  /* @ngdoc object
   * @name components.admin
   * @description
   *
   */
  angular
    .module('components.admin', [
      'admin.productsFooterCommon',
      'admin.footer',
      'admin.products'
    ]);
}());
