(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name admin.productsFooterCommon.factory:AdminCommon
   *
   * @description
   *
   */
  angular
    .module('admin.productsFooterCommon')
    .factory('AdminCommon', AdminCommon);

  function AdminCommon() {
    var vm = {};

    vm.dragged = false;

    function uid() {
      return new Date().getTime() +
        Math.floor(Math.random() * (1000 + 1));
    }

    vm.save = function(item, arr){
      if (item.id == null) {
        //if this is new, add it in array
        item.id = uid();
        arr.push(item);
      } else {
        //for existing, find this using id
        //and update it.
        for (var i = 0; i < arr.length; i++) {
          if (arr[i].id == item.id) {
            arr[i] = item;
          }
        }
      }
    }

    vm.deleteById = function(id, arr){
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].id == id) {
          arr.splice(i, 1);
        }
      }
    }

    vm.deleteByItem = function(item, arr){
      var index = arr.indexOf(item);
      arr.splice(index, 1);
    }

    return vm;
  }
}());
