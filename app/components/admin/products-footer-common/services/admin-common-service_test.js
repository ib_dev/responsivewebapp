/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AdminCommon', function () {
  var service;

  beforeEach(module('admin.productsFooterCommon'));

  beforeEach(inject(function (AdminCommon) {
    service = AdminCommon;
  }));

  it('should equal AdminCommon', function () {
    expect(service.get()).toEqual('AdminCommon');
  });
});
