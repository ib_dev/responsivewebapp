(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name admin.productsFooterCommon.directive:customClick
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="admin.productsFooterCommon">
       <file name="index.html">
        <custom-click></custom-click>
       </file>
     </example>
   *
   */
  angular
    .module('admin.productsFooterCommon')
    .directive('customClick', customClick);

  function customClick(AdminCommon, $timeout) {
    return {
      restrict: "A",
      scope: {
        'customClick': '&'
      },
      link: function(scope, el, attrs){
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */

        el.on( 'mousedown', onMouseDown );
        el.on( 'mouseup', onMouseUp );

        function onMouseDown() {
          AdminCommon.dragged = false;
        }

        function onMouseUp() {

          if( AdminCommon.dragged ) return;

          if (typeof attrs.customClickClass === 'string') addRemoveClass();

          $timeout(function() { // smooth scroll not working without delay
            scope.customClick();
          }, 1);

        }

        function addRemoveClass() {

          if( el.hasClass('disabled') ) return;

          var cssClass = attrs.customClickClass;

          el.siblings().removeClass(cssClass);

          el.addClass(cssClass);

          el.bind("clickoutside", function() {
            el.removeClass(cssClass);
            el.unbind("clickoutside");
          });

        };

      }
    }
  }
}());
