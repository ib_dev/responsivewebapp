/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('customClick', function () {
  var scope
    , element;

  beforeEach(module('admin.productsFooterCommon', 'components/admin/products-footer-common/directives/custom-click-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<custom-click></custom-click>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().customClick.name).toEqual('customClick');
  });
});
