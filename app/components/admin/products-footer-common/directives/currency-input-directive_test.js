/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('currencyInput', function () {
  var scope
    , element;

  beforeEach(module('admin.productsFooterCommon', 'components/admin/products-footer-common/directives/currency-input-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<currency-input></currency-input>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().currencyInput.name).toEqual('currencyInput');
  });
});
