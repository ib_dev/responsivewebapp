(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name admin.productsFooterCommon.directive:currencyInput
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="admin.productsFooterCommon">
       <file name="index.html">
        <currency-input></currency-input>
       </file>
     </example>
   *
   */
  angular
    .module('admin.productsFooterCommon')
    .directive('currencyInput', currencyInput);

  function currencyInput($filter) {
    return {
      restrict: "A",
      scope: {
        amount: '=',
        disable: '='
      },
      link: function (scope, el, attrs) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */

        function addCurrency(){
          return el.val( $filter('addCurrencySymbol')(scope.amount,true) );
        }

        addCurrency();

        el.bind('focus', function(){
          el.val(scope.amount);
        });

        el.bind('input', function(){
          scope.$apply(function () {
            scope.amount = el.val();
          });
        });

        el.bind('blur', function(){
          addCurrency();
        });

        scope.$watch('disable', function(value) {
          if(value) addCurrency();
        });

      }
    };
  }
}());
