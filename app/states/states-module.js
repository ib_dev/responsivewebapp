(function () {
  'use strict';

  /* @ngdoc object
   * @name states
   * @description
   *
   */
  angular
    .module('states', [
      'states.home',
      'states.admin',
      'states.homeAdminCommon'
    ]);
}());
