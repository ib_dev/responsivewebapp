(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name states.admin.controller:AdminCtrl
   *
   * @description
   *
   */
  angular
    .module('states.admin')
    .controller('AdminCtrl', AdminCtrl);

  function AdminCtrl($controller, data) {
    var vm = this;

    vm.disable = false;

    // instantiate base controller
    $controller('HomeAdminMainBaseCtrl', { vm: vm, data: data });
  }
}());
