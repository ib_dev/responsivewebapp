(function () {
  'use strict';

  angular
    .module('states.admin')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('admin', {
        url: '/admin',
        templateUrl: 'states/admin/views/admin.tpl.html',
        controller: 'AdminCtrl',
        controllerAs: 'vm',
        resolve: {
          data: ['GetData', function(GetData) {
            return GetData;
          }]
        }
      });
  }
}());
