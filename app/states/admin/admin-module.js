(function () {
  'use strict';

  /* @ngdoc object
   * @name states.admin
   * @description
   *
   */
  angular
    .module('states.admin', [
      'ui.router'
    ]);
}());
