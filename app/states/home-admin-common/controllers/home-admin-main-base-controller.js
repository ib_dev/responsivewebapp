(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name states.homeAdminCommon.controller:HomeAdminMainBaseCtrl
   *
   * @description
   *
   */
  angular
    .module('states.homeAdminCommon')
    .controller('HomeAdminMainBaseCtrl', HomeAdminMainBaseCtrl);

  function HomeAdminMainBaseCtrl(vm, data) {
    vm.productsData = data.products();
    vm.footerData = data.footer();
  }
}());
