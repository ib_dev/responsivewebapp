/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('HomeAdminMainBaseCtrl', function () {
  var ctrl;

  beforeEach(module('states.homeAdminCommon'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('HomeAdminMainBaseCtrl');
  }));

  it('should have ctrlName as HomeAdminMainBaseCtrl', function () {
    expect(ctrl.ctrlName).toEqual('HomeAdminMainBaseCtrl');
  });
});
