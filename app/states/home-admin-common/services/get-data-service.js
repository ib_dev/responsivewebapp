(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name states.homeAdminCommon.factory:GetData
   *
   * @description
   *
   */
  angular
    .module('states.homeAdminCommon')
    .factory('GetData', GetData);

  function GetData($q, $http) {
    var deferred = $q.defer();
    $http.get('http://127.0.0.1:8081/data').success(function(response) { // NODE.JS

      deferred.resolve({
        footer: function(){
          return response[0];
        },
        products: function(){
          return response[1];
        }
      });

    }).error(function(response) {
      console.log('ERROR: ', response);
      deferred.reject(response);
    });

    return deferred.promise;
  }
}());
