/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('GetData', function () {
  var service;

  beforeEach(module('states.homeAdminCommon'));

  beforeEach(inject(function (GetData) {
    service = GetData;
  }));

  it('should equal GetData', function () {
    expect(service.get()).toEqual('GetData');
  });
});
