(function () {
  'use strict';

  /* @ngdoc object
   * @name states.home
   * @description
   *
   */
  angular
    .module('states.home', [
      'ui.router'
    ]);
}());
