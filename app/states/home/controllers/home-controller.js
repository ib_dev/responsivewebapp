(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name states.home.controller:HomeCtrl
   *
   * @description
   *
   */
  angular
    .module('states.home')
    .controller('HomeCtrl', HomeCtrl);

  function HomeCtrl($controller, data) {
    var vm = this;

    // instantiate base controller
    $controller('HomeAdminMainBaseCtrl', { vm: vm, data: data });
  }
}());
