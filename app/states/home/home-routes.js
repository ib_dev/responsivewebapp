(function () {
  'use strict';

  angular
    .module('states.home')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'states/home/views/home.tpl.html',
        controller: 'HomeCtrl',
        controllerAs: 'vm',
        resolve: {
          data: ['GetData', function(GetData) {
            return GetData;
          }]
        }
      });
  }
}());
