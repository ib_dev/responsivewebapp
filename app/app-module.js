(function () {
  'use strict';

  /* @ngdoc object
   * @name app
   * @description
   *
   */
  angular
    .module('app', [
      'ui.router',
      'ui.bootstrap',
      'ui.sortable',
      'ngAnimate',
      'duScroll',
      'states',
      'components'
    ]);
}());
